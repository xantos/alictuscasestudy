﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    Vector3 targetPosition, lookAtTarget;
    Quaternion characterRotation;
    public float speed = 6.0f;
    public float rotationSpeed = 4.0f;
    bool moving = false;
    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButton(0)) {
            SetTargetPosition();
        }

        if(moving) {
            CharacterMove();
        }
    }

    void SetTargetPosition() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 1000)) {
            targetPosition = hit.point;
            lookAtTarget = new Vector3(targetPosition.x - transform.position.x, transform.position.y, targetPosition.z - transform.position.z);
            characterRotation = Quaternion.LookRotation(lookAtTarget);
            moving = true;
        }
    }

    void CharacterMove() {
        transform.rotation = Quaternion.Slerp(transform.rotation, characterRotation, rotationSpeed * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        if(Vector3.Distance(transform.position, targetPosition) <= 0.5f) {
            moving = false;
        }
    }
}
