﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyerController : MonoBehaviour
{
    private GameObject owlObject;
    private Text txt, destroyedCubesText, totalCubesText;
    private int totalCube, destroyedCube = 0;
    // Start is called before the first frame update
    void Start() {
        totalCube = GameObject.FindGameObjectsWithTag("owl").Length;
        //totalCubeText = texts.GetComponent<Text>();
        /*if(canvas.tag == "text") {
            string asd = canvas.
        }*/
        txt = GameObject.Find("Canvas/Text").GetComponent<Text>();
        totalCubesText = GameObject.Find("Canvas/totalCubesText").GetComponent<Text>();
        destroyedCubesText = GameObject.Find("Canvas/destroyedCubesText").GetComponent<Text>();
        totalCubesText.text = totalCube.ToString();
    }

    // Update is called once per frame
    void Update() {
        destroyedCubesText.text = destroyedCube.ToString();
    }

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.tag == "owl") {
            owlObject = other.gameObject;
            Destroy(owlObject);
            destroyedCube++;
        }
    }
}
